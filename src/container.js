import { LitElement, html } from '@polymer/lit-element'

import './LitRossi'

class ContainerElement extends LitElement {
  _render(props) {
    return html`
<h1>Hello World</h1>
<rossi-element></rossi-element>
`
  }

}

customElements.define('container-element', ContainerElement)
